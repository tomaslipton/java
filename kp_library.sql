-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 05 2018 г., 01:30
-- Версия сервера: 5.6.38
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `kp_libraryDatabase`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Book`
--

CREATE TABLE `Book` (
  `BookID` int(11) UNSIGNED NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Author` varchar(50) NOT NULL,
  `Genre` varchar(30) NOT NULL,
  `PublishingHouse` varchar(50) DEFAULT NULL,
  `Year` int(10) UNSIGNED DEFAULT NULL,
  `Pages` int(10) UNSIGNED DEFAULT NULL,
  `Quantity` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Book`
--

INSERT INTO `Book` (`BookID`, `Title`, `Author`, `Genre`, `PublishingHouse`, `Year`, `Pages`, `Quantity`) VALUES
(7, 'TEst', 'test', 'Каталог', 'qwe', 9999, 60, 8),
(8, 'Книга', 'Пушкин', 'Домашняя', 'Офсет', 1999, 300, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `OrderedBooks`
--

CREATE TABLE `OrderedBooks` (
  `PersonID` int(11) NOT NULL,
  `BookID` int(11) UNSIGNED NOT NULL,
  `OrderDate` date NOT NULL,
  `ReturnDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `OrderedBooks`
--

INSERT INTO `OrderedBooks` (`PersonID`, `BookID`, `OrderDate`, `ReturnDate`) VALUES
(19, 7, '2018-12-05', '2018-12-19'),
(21, 7, '2018-12-05', '2018-12-19'),
(21, 8, '2018-12-05', '2018-12-19');

-- --------------------------------------------------------

--
-- Структура таблицы `person`
--

CREATE TABLE `person` (
  `PersonID` int(11) NOT NULL,
  `Name` varchar(25) NOT NULL,
  `Surname` varchar(25) NOT NULL,
  `Email` varchar(40) CHARACTER SET latin1 NOT NULL,
  `Password` varchar(25) CHARACTER SET latin1 NOT NULL,
  `RegistrationDate` date DEFAULT NULL,
  `Gender` enum('MAN','WOMAN') CHARACTER SET latin1 DEFAULT NULL,
  `isLibrarian` varchar(45) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `person`
--

INSERT INTO `person` (`PersonID`, `Name`, `Surname`, `Email`, `Password`, `RegistrationDate`, `Gender`, `isLibrarian`) VALUES
(19, 'asd', 'qwe', 'asd@qwe.cc', '1111111', '2018-12-03', 'MAN', '0'),
(20, 'Vvv', 'yyyy', 'asd@asd.com', 'qweqwe', '2018-12-05', 'MAN', '0'),
(21, 'Vova', 'Yakovenko', 'tomass@gmail.com', 'asdasdasd', '2018-12-05', 'WOMAN', '1');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Book`
--
ALTER TABLE `Book`
  ADD PRIMARY KEY (`BookID`);

--
-- Индексы таблицы `OrderedBooks`
--
ALTER TABLE `OrderedBooks`
  ADD PRIMARY KEY (`PersonID`,`BookID`),
  ADD KEY `fk_OrderedBooks_Person_idx` (`PersonID`),
  ADD KEY `fk_OrderedBooks_Book1_idx` (`BookID`);

--
-- Индексы таблицы `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`PersonID`),
  ADD UNIQUE KEY `Email_UNIQUE` (`Email`),
  ADD UNIQUE KEY `PersonID_UNIQUE` (`PersonID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Book`
--
ALTER TABLE `Book`
  MODIFY `BookID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `person`
--
ALTER TABLE `person`
  MODIFY `PersonID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `OrderedBooks`
--
ALTER TABLE `OrderedBooks`
  ADD CONSTRAINT `fk_OrderedBooks_Book1` FOREIGN KEY (`BookID`) REFERENCES `book` (`BookID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_OrderedBooks_Person` FOREIGN KEY (`PersonID`) REFERENCES `person` (`PersonID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
