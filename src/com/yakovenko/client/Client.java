package com.yakovenko.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client extends Application {

    private static ObjectInputStream clientReader;
    private static ObjectOutputStream clientWriter;
    private String kp_libraryLogo = "resources/Logo.png";
    private static Socket serverSocket;

    @Override
    public void start(Stage mainStage) throws Exception {
        serverSocket = new Socket("localhost", 4242);
        clientWriter = new ObjectOutputStream(serverSocket.getOutputStream());
        clientReader = new ObjectInputStream(serverSocket.getInputStream());
        Parent root = FXMLLoader.load(getClass().getResource("scenes/StartPage.fxml"));
        mainStage.setTitle("Система учета новых поступлений в библиотеку");
        mainStage.getIcons().add(new Image(getClass().getResourceAsStream(kp_libraryLogo)));
        mainStage.setScene(new Scene(root, 900, 560));
        mainStage.setResizable(false);
        mainStage.show();
    }

    @Override
    public void stop() throws IOException {
        clientWriter.writeInt(-1);
        clientWriter.flush();
    }

    public static ObjectInputStream getClientReader() {
        return clientReader;
    }

    public static ObjectOutputStream getClientWriter() {
        return clientWriter;
    }

    public static Socket getServerSocket() {
        return serverSocket;
    }

    public static void main(String[] args) {
        launch(args);
    }
}