package com.yakovenko.entity;

import java.io.Serializable;

/**
 * Created by pig48 on 09.12.2018.
 */
public class BookGenre implements Serializable {

    public BookGenre(int idgenre, String name){
        this.idgenre=idgenre;
        this.name=name;
    }

    private int idgenre;
    private String name;

//    @Override
//    public String toString(){
//        return "Название: " + this.title + "\nАвтор: " + this.author + "\nЖанр: " + this.genre.toString()
//                + "\nИздательство: " + this.publishingHouse + "\nГод издания: " + this.year + "\nКоличество страниц: "
//                + this.pages + "\nКоличество экземпляров: " + this.quantity + "\n";
//    }

    public String getGenreName() {
        return name;
    }

    public int getGenreID() {
        return idgenre;
    }
}
